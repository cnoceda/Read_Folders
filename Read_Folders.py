"""Guarda la información de los ficheros.

Recorre los directorios y almacena toda la información disponible de estos.
Ademas trata de crear un hash para saber si las imágenes se repiten.
Permite el movimiento de los ficheros de video a un directorio aparte.
"""

import os
import shutil
import logging
import argparse
import time
from datetime import datetime
from dateutil.parser import parse

from functools import reduce
from PIL import Image
from pillow_heif import register_heif_opener
import exiftool
import re

import sqlite3
from sqlite3 import Error
import imagehash

Image.warnings.simplefilter('error', Image.DecompressionBombWarning)

# Be careful with this setting, disables the DecompressionBombWarning
Image.MAX_IMAGE_PIXELS = None

register_heif_opener()

start_time = time.time()
parser = argparse.ArgumentParser(description='Creates a DB with all the '
                                 'files in the folder and move the Videos'
                                 ' to an other folder.',
                                 prog='Read_Folders',
                                 usage='%(prog)s [options]',
                                 formatter_class=argparse
                                 .ArgumentDefaultsHelpFormatter)

requiredNamed = parser.add_argument_group('required named arguments')

requiredNamed.add_argument('-s', '--source', help='Source folder',
                           required=True)

parser.add_argument('-m', '--move', help='Activates the movement to the Video '
                    'and Other files to the destination folder.',
                    action='store_true')

parser.add_argument('-d', '--destination', help='Destination folder. '
                    'Mandatory if -m is True.')

parser.add_argument('-hs', '--hashfunc', help='Hash function to use. '
                    'imagehash: phash, dhash, whash-haar, colorhash, '
                    'crop-resistant.', default=None)

parser.add_argument('--clearLog', help='Deletes the old Log File.',
                    action='store_true')

iPictures_Found = 0
et = exiftool.ExifToolHelper()


def main(f_walk, f_destination, moving=False,
         hashfunc=None):
    """Función principal de la aplicación."""
    global iPictures_Found
    walk_dir = f_walk
    dest_base_dir = f_destination
    iMovies_moved = 0
    file_counters = {
        "Video": 0,
        "Other": 0,
        "Image": 0,
        "Image Raw": 0,
        "Size":  0
    }
    bMove = moving
    exclude_dirs = set(['.@__thumb', '@Recycle'])

    logging.info('Origen = ' + walk_dir)
    logging.info('Origen (absolute) = ' + os.path.abspath(walk_dir))
    logging.info('Destino = ' + dest_base_dir)
    logging.info('Destino (absolute) = ' + os.path.abspath(dest_base_dir))

    if bMove:
        ensure_dir(dest_base_dir)

    db_file = "/home/cnoceda/Downloads/" + walk_dir[1:].replace('/', '_')
    + 'RF.sqlite'
    print('Base de datos en: ' + db_file)
    logging.info('Base de datos en ' + db_file)
    dbObj = MyDB(db_file)
    # dbObj.ClearDB(walk_dir)

    for root, subdirs, files in os.walk(walk_dir):
        subdirs[:] = [d for d in subdirs if d not in exclude_dirs]
        logging.info('- root dir = ' + root)
        rel_dir = os.path.relpath(root, walk_dir)
        dest_rootdir = os.path.join(dest_base_dir, rel_dir)
        if bMove:
            ensure_dir(dest_rootdir)

        if not dbObj.check_folder(root):

            for filename in files:
                file_path = os.path.join(root, filename)
                file_dest = os.path.abspath(os.path.join(dest_rootdir,
                                                         filename))
                logging.info('- File %s' % (file_path))
                start_time = time.time()
                file_type = CheckFiles(file_path, dbObj, hashfunc)
                logging.info('  - Process time %s (is: %s)' % (time.time()
                                                               - start_time,
                                                               file_type))
                file_counters[file_type] = file_counters[file_type] + 1
                file_counters["Size"] = file_counters["Size"]
                + os.path.getsize(file_path)

                if file_type in ['Video', 'Other'] and bMove:
                    shutil.move(file_path, file_dest)
                    iMovies_moved = iMovies_moved + 1
                    logging.info('movemos %s numero %s'
                                 % (file_path, str(iMovies_moved)))

            dbObj.insertFolder(root, os.path.basename(os.path.normpath(root)),
                               dbObj.GetInsert(),
                               file_counters["Image"]
                               + file_counters["Image Raw"],
                               file_counters["Video"],
                               file_counters["Other"],
                               file_counters["Size"])

            dbObj.DBCommit()

            logging.info('Insertados = ' + str(dbObj.GetInsert())
                         + ' en ' + root)
            print('    Insertados = ' + str(dbObj.GetInsert())
                  + ' en ' + root)
            dbObj.ClearCounter()
            file_counters = file_counters.fromkeys(file_counters, 0)
        else:
            logging.info('Saltando la carpeta ' +
                         root + 'porque existe en la BBDD.')

    logging.info('FINISHING HERE. Moved='
                 + str(iMovies_moved)
                 + ' Pic= '
                 + str(iPictures_Found)
                 + ' Insert= ' + dbObj.GetInsert())


def secondsToStr(t):
    """Convierte los segundos en legibles."""
    return "%d:%02d:%02d.%03d" % \
        reduce(lambda ll, b: divmod(ll[0], b) + ll[1:],
               [(t*1000,), 1000, 60, 60])


def try_parsing_date(text):
    text = "1970:01:01 00:00:00" if text == '0000:00:00 00:00:00' else text
    for fmt in ('%Y:%m:%d %H:%M:%S',
                '%Y:%m:%d %H:%M:%S%z',
                '%Y-%m-%d %H:%M:%S',
                '%Y.%m.%d %H:%M:%S',
                '%Y/%m/%d %H:%M:%S',
                'UTC %Y-%m-%d %H:%M:%S'):
        try:
            return datetime.strptime(text, fmt)
        except ValueError:
            print(text)
            pass
    raise ValueError('no valid date format found.')


def ensure_dir(file_path):
    """Chequea que el directorio exista."""
    if not os.path.exists(file_path):
        logging.warning('- No existe filepath: ' + file_path)
        os.makedirs(file_path)


def CheckFiles(fpath, db, hashfunc):
    """Comprueba todos los ficheros."""
    global iPictures_Found
    global et

    iType = 'Other'
    try:
        metadata = et.get_tags(fpath, "*mime*")
        ftype = list(metadata[0].values())[1] if len(metadata[0].keys()) > 1 else "OTH"
    except exiftool.exceptions.ExifToolExecuteError:
        ftype = "OTH"
        pass

    filename, file_extension = os.path.splitext(fpath)

    if ftype:
        if "image" in ftype:
            # print("imagen ", fpath)
            iType = 'Image'
            PutImage(db, fpath, hashfunc)
            iPictures_Found = iPictures_Found + 1
        elif "video" in ftype:
            # print("video ", fpath)
            iType = 'Video'
            PutVideo(db, fpath)
        elif "application" in ftype:
            # print("app ", fpath)
            PutVideo(db, fpath)
        else:
            # print("other ", fpath)
            PutOther(db, fpath)

    return iType


def PutImage(db, path, hashfunc):
    "Mete el fichero de Imagen en la DDBB."
    global et
    filename, file_extension = os.path.splitext(path)
    statinfo = os.stat(path)
    img_inf = GetVideoExif(path)
    metadata = et.get_metadata(path)

    if hashfunc and file_extension == '.jpg':
        img = Image.open(path)
        img_hash = hashfunc(img)
        db.insertIMG(img_inf['encoded_date'],
                     os.path.basename(path),
                     os.path.dirname(path),
                     img_inf['type'],
                     statinfo.st_size,
                     img_inf['width'],
                     img_inf['height'],
                     path,
                     str(img_hash),
                     str(hashfunc),
                     str(metadata[0]))

    else:
        img_hash = "Sin Hash"
        hashfunc = "Sin Func"
        db.insertIMG(img_inf['encoded_date'],
                     os.path.basename(path),
                     os.path.dirname(path),
                     img_inf['type'],
                     statinfo.st_size,
                     img_inf['width'],
                     img_inf['height'],
                     path,
                     str(metadata[0]))


def PutVideo(db, fpath):
    global et

    statinfo = os.stat(fpath)
    video_inf = GetVideoExif(fpath)
    metadata = et.get_metadata(fpath)

    db.insertVID(video_inf['encoded_date'],
                 os.path.basename(fpath),
                 os.path.dirname(fpath),
                 video_inf['type'],
                 video_inf['width'],
                 video_inf['height'],
                 str(statinfo.st_size),
                 fpath,
                 str(metadata[0]))


def PutOther(db, fpath):
    statinfo = os.stat(fpath)
    filename, file_extension = os.path.splitext(fpath)
    db.insertOTH(os.path.basename(fpath),
                 os.path.dirname(fpath),
                 statinfo.st_size,
                 fpath,
                 file_extension)


def GetVideoExif(path):
    global et
    date_tags = ["DateTimeOriginal", "CreateDate", "FileModifyDate"]
    width_tags = ["imageWidth"]
    height_tags = ["imageheight"]
    metadata = et.get_tags(path,
                           tags=["DateTimeOriginal",
                                 "CreateDate",
                                 "ImageWidth",
                                 "ImageHeight",
                                 "FileModifyDate",
                                 "FileType"])
    info = {'encoded_date': "1970:01:01 00:00:00",
            'width': 0,
            'height': 0,
            'type': "OTR"}

    t = 'FileType'
    for k, v in metadata[0].items():
        if re.search(t, k, re.IGNORECASE):
            info['type'] = v
            break

    # Buscar fecha
    f = False
    i = 0
    while not f and i < len(date_tags):
        t = date_tags[i]
        for k, v in metadata[0].items():
            if re.search(t, k, re.IGNORECASE):
                info['encoded_date'] = v
                f = True
                break
        i = i + 1

    # Buscar width
    f = False
    i = 0
    while not f and i < len(width_tags):
        t = width_tags[i]
        for k, v in metadata[0].items():
            if re.search(t, k, re.IGNORECASE):
                info['width'] = v
                f = True
                break
        i = i + 1

    # Buscar height
    f = False
    i = 0
    while not f and i < len(height_tags):
        t = height_tags[i]
        for k, v in metadata[0].items():
            if re.search(t, k, re.IGNORECASE):
                info['height'] = v
                f = True
                break
        i = i + 1

    if info['encoded_date'] != "":
        try:
            d = parse(info['encoded_date'][:10].replace(':', '-')
                      + info['encoded_date'][10:])
        except:
            d = parse("1970-01-01 00:00:00")
            pass
        info['encoded_date'] = d.strftime("%Y%m%dT%H%M%S")

    return info


class MyDB():
    """Clase para manejar la BBDD."""

    _db_connection = None
    _db_cur = None
    _count_Insert = 0
    _count_Folders = 0
    _date_insert = datetime.now().strftime('%Y%m%d')

    def __init__(self, db_file):
        """Iniciador de la clase como parametro el nombre del fichero."""
        try:
            self._db_connection = sqlite3.connect(db_file)
            self._db_cur = self._db_connection.cursor()
        except Error as e:
            print(e)
        finally:
            self.createTables()

    def createTables(self):
        """Crea la tabla necesaria para guardar la info de los ficheros."""
        sql_create_fm_table = """
        CREATE TABLE IF NOT EXISTS ficheros_multimedia (
            file_id INTEGER PRIMARY KEY,
            insert_date TEXT NOT_NULL,
            capture_date TEXT NOT_NULL,
            file_name TEXT NOT NULL,
            file_folder TEXT NOT NULL,
            file_type TEXT NOT NULL,
            file_size NTEGER,
            Width INTEGER,
            Height INTEGER,
            file_path TEXT,
            hash_func TEXT,
            img_hash TEXT,
            exif_data TEXT
            );"""
        self._db_cur.execute(sql_create_fm_table)

        sql_create_folder_table = """
        CREATE TABLE IF NOT EXISTS carpetas_multimedia (
            folder_id INTEGER PRIMARY KEY,
            insert_date TEXT NOT_NULL,
            full_name TEXT NOT NULL,
            name_folder TEXT NOT NULL,
            Total_files INTEGER,
            IMG_files INTEGER,
            VIDEO_files INTEGER,
            OTHER_files INTEGER,
            folder_size NTEGER
            );"""
        self._db_cur.execute(sql_create_folder_table)

    def query(self, query, params):
        """Ejecuta una query en la BBDD."""
        return self._db_cur.execute(query, params)

    def check_folder(self, folder):
        """Comprueba si la carpeta ha sido ya analizada."""
        self._db_cur.execute("SELECT count(*)"
                             "FROM carpetas_multimedia"
                             "WHERE full_name=?;", (folder,))
        data = self._db_cur.fetchone()[0]
        existe = True

        if data == 0:
            print('Procesando %s ...' % folder)
            existe = False
        else:
            print('La carpeta %s existe en la BBDD.' % folder)
        return existe

    def insertFolder(self, f_name, n_folder, t_files, i_files, v_files,
                     o_files, f_size=0):
        """Inserta los datos recogidos de la carpeta."""
        sql = ("""INSERT INTO `carpetas_multimedia`
                (`insert_date`, `full_name`, `name_folder`,
                `Total_files`, `IMG_files`,
                `VIDEO_files`, `OTHER_files`, `folder_size`)
                VALUES (?, ?, ?, ? , ? , ? ,? , ?);""")

        self._db_cur.execute(sql, (self._date_insert,
                                   f_name,
                                   n_folder,
                                   t_files,
                                   i_files,
                                   v_files,
                                   o_files,
                                   f_size))
        self._count_Folders = self._count_Folders + 1

    def insertIMG(self, txt_date, f_name, f_folder, f_format, f_size,
                  i_width, i_height, f_path, e_data, i_hash='', hash_func=''):
        """Inserta los datos recogidos de la imagen en la BBDD."""
        sql = ("""INSERT INTO `ficheros_multimedia`
               (`capture_date`, `insert_date`, `file_name`,
                `file_folder`, `file_type`, `file_size`,
                `Width`, `Height`, `file_path`, `img_hash`,
                `hash_func`, `exif_data`)
               VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);""")
        self._db_cur.execute(sql, (txt_date,
                                   self._date_insert,
                                   f_name,
                                   f_folder,
                                   f_format,
                                   f_size,
                                   i_width,
                                   i_height,
                                   f_path,
                                   i_hash,
                                   hash_func,
                                   e_data))
        self._count_Insert = self._count_Insert + 1

    def insertVID(self, txt_date, f_name, f_folder, f_type,
                  i_width, i_height, f_size, f_path, e_data):
        """Inserta los datos recogidos de un fichero de video en la BBDD."""
        sql = ("""INSERT INTO `ficheros_multimedia`
                   (`capture_date`, `insert_date`, `file_name`,
                    `file_folder`, `file_type`,`file_size`,
                    `Width`, `Height`,`file_path`, `exif_data`)
                    VALUES (?, ? , ?, ?, ?, ?, ?, ?, ?, ?);""")
        Params = (txt_date, self._date_insert, f_name, f_folder, f_type,
                  f_size, i_width, i_height, f_path, e_data)

        self._db_cur.execute(sql, Params)
        self._count_Insert = self._count_Insert + 1

    def insertOTH(self, f_name, f_folder,  f_size,  f_path, f_extension):
        """Inserta la información de un fichero no multimedia en la BBDD."""
        sql = ("""INSERT INTO `ficheros_multimedia`
                (`insert_date`, `file_name`, `file_folder`, `file_type`,
                 `file_size`,`file_path`)
                 VALUES (?, ?, ?, ?, ?, ?);""")
        self._db_cur.execute(sql, (self._date_insert,
                                   f_name,
                                   f_folder,
                                   f_extension.upper(),
                                   f_size,
                                   f_path))
        if f_path != 'folder':
            self._count_Insert = self._count_Insert + 1

    def ClearDB(self, fhome):
        """Borra la BBDD."""
        sql = (f'DELETE FROM `ficheros_multimedia`'
               f' WHERE file_folder like "{fhome}%";')
        self._db_cur.execute(sql)
        self._db_connection.commit()

        sql = (f'DELETE FROM `carpetas_multimedia`'
               f' WHERE full_name like "{fhome}%";')
        self._db_cur.execute(sql)
        self._db_connection.commit()

    def GetInsert(self):
        """Devuelve el numero de registros insertados."""
        return str(self._count_Insert)

    def GetInsertFolders(self):
        """Devuelve el numero de registros insertados."""
        return str(self._count_Folders)

    def ClearCounter(self):
        """Resetea el contador."""
        self._count_Insert = 0
        self._count_Folders = 0

    def DBCommit(self):
        """Realiza el commit de la BBDD."""
        self._db_connection.commit()

    def __del__(self):
        """Destruye la instancia."""
        self._db_connection.close()


if __name__ == "__main__":
    args = parser.parse_args()
    if args.move and not args.destination:
        print('Please give me a destination folder')
        parser.print_help()
    else:
        if args.clearLog and os.path.exists(args.source + '/Read_Folders.log'):
            os.remove(args.source + '/Read_Folders.log')

        logging.basicConfig(filename=args.source[1:].replace('/', '_') +
                            'RF.log',
                            level=logging.INFO,
                            format='%(asctime)s -- %(levelname)s: %(message)s')
        logging.info('STARTING HERE')
        logging.info('PARAMETERS => ' + str(args))

        hashfunc = None
        hashmethod = args.hashfunc
        if hashmethod == 'ahash':
            hashfunc = imagehash.average_hash
        elif hashmethod == 'phash':
            hashfunc = imagehash.phash
        elif hashmethod == 'dhash':
            hashfunc = imagehash.dhash
        elif hashmethod == 'whash-haar':
            hashfunc = imagehash.whash
        elif hashmethod == 'colorhash':
            hashfunc = imagehash.colorhash
        elif hashmethod == 'crop-resistant':
            hashfunc = imagehash.crop_resistant_hash

        print("Init hash:", hashfunc)
        if not args.destination:
            main(args.source, '', args.move, hashfunc)
        else:
            main(args.source, args.destination, args.move, hashfunc)

        logging.info('TIME OF EXECUTION = '
                     + secondsToStr(time.time() - start_time))
