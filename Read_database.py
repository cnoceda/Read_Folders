"""Lee la base de datos creada con Read_Folders.

Buscando los posibles duplicados
"""

# import os
import argparse
import time
from functools import reduce
import logging
import sqlite3
from sqlite3 import Error

start_time = time.time()
parser = argparse.ArgumentParser(description='Lee la base de datos creada por'
                                 ' Read_Folders.py',
                                 prog='Read_database',
                                 usage='%(prog)s [options]',
                                 formatter_class=argparse
                                 .ArgumentDefaultsHelpFormatter)
requiredNamed = parser.add_argument_group('requiered named arguments')
requiredNamed.add_argument('-s', '--source', help='Database to read',
                           required=True)


def main(db_file):
    """Función principal de la aplicación."""
    # iDeleted_files = 0
    logging.info('Database = ' + db_file)
    dbObj = MyDB(db_file)
    logging.info('Nº registros = ' + str(dbObj.GetRows()))


def secondsToStr(t):
    """Convierte los segundos en legibles."""
    return "%d:%02d:%02d.%03d" % \
        reduce(lambda ll, b: divmod(ll[0], b) + ll[1:],
               [(t*1000,), 1000, 60, 60])


class MyDB():
    """Clase para manejar la BBDD."""

    _db_connection = None
    _db_cur = None
    _count_Insert = 0

    def __init__(self, db_file):
        """Iniciador de la clase como parametro el nombre del fichero."""
        try:
            self._db_connection = sqlite3.connect(db_file)
            self._db_cur = self._db_connection.cursor()
        except Error as e:
            print(e)
        finally:
            # get the count of tables with the name
            self._db_cur.execute("SELECT count(name)"
                                 "FROM sqlite_master"
                                 "WHERE type='table' "
                                 "AND name='ficheros_multimedia'")
            # if the count is 1, then table exists
            if self._db_cur.fetchone()[0] != 1:
                self.createTables()

    def createTables(self):
        """Crea la tabla necesaria para guardar la info de los ficheros."""
        sql_create_fm_table = """
        CREATE TABLE IF NOT EXISTS ficheros_multimedia
            (file_name TEXT NOT NULL,
            file_folder TEXT NOT NULL,
            file_type TEXT NOT NULL,
            file_size NTEGER,
            Width INTEGER,
            Height INTEGER,
            file_path TEXT,
            hash_func TEXT,
            img_hash TEXT
            );"""
        self._db_cur.execute(sql_create_fm_table)

    def query(self, query, params):
        """Ejecuta una query en la BBDD."""
        return self._db_cur.execute(query, params)

    def insertIMG(self, f_name, f_folder, f_format, f_size,
                  i_width, i_height, f_path, i_hash='', hash_func=''):
        """Inserta los datos recogidos de la imagen en la BBDD."""
        sql = (f"INSERT INTO `ficheros_multimedia`"
               f" (`file_name`, `file_folder`, `file_type`, `file_size`,"
               f" `Width`, `Height`, `file_path`, `img_hash`, `hash_func`)"
               f" VALUES ('{f_name}', '{f_folder}', '{f_format}',{f_size},"
               f"{i_width},{i_height},'{f_path}', '{i_hash}', '{hash_func}');")
        self._db_cur.execute(sql)
        self._count_Insert = self._count_Insert + 1

    def insertVID(self, f_name, f_folder,  f_size,  f_path):
        """Inserta los datos recogidos de un fichero de video en la BBDD."""
        sql = (f"INSERT INTO `ficheros_multimedia`"
               f" (`file_name`, `file_folder`, `file_type`,"
               f" `file_size`,`file_path`)"
               f" VALUES ('{f_name}', '{f_folder}', 'VIDEO',"
               f" {f_size},'{f_path}')")
        self._db_cur.execute(sql)
        self._count_Insert = self._count_Insert + 1

    def insertOTH(self, f_name, f_folder,  f_size,  f_path, f_extension):
        """Inserta la información de un fichero no multimedia en la BBDD."""
        sql = (f"INSERT INTO `ficheros_multimedia`"
               f" (`file_name`, `file_folder`, `file_type`,"
               f" `file_size`,`file_path`)"
               f" VALUES ('{f_name}', '{f_folder}', '{f_extension.upper()}',"
               f" {f_size},'{f_path}')")
        self._db_cur.execute(sql)
        if f_path != 'folder':
            self._count_Insert = self._count_Insert + 1

    def ClearDB(self, fhome):
        """Borra la BBDD."""
        sql = (f'DELETE FROM `ficheros_multimedia`'
               f' WHERE file_folder like "{fhome}%";')
        self._db_cur.execute(sql)
        self._db_connection.commit()

    def GetInsert(self):
        """Devuelve el numero de registros insertados."""
        return str(self._count_Insert)

    def GetRows(self):
        """Devuelve el numero de registros insertados."""
        rowsQuery = "SELECT Count() FROM ficheros_multimedia"
        self._db_cur.execute(rowsQuery)
        numberOfRows = self._db_cur.fetchone()[0]
        return numberOfRows

    def GetDup_Folders(self):
        """Devuelve las posibles carpetas duplicadas."""
        sql_dup = ""

    def ClearCounter(self):
        """Resetea el contador."""
        self._count_Insert = 0

    def DBCommit(self):
        """Realiza el commit de la BBDD."""
        self._db_connection.commit()

    def __del__(self):
        """Destruye la instancia."""
        self._db_connection.close()


if __name__ == "__main__":
    args = parser.parse_args()
    logging.basicConfig(filename=args.source + '/Read_database.log',
                        level=logging.INFO,
                        format='%(asctime)s -- %(levelname)s: %(message)s')
    logging.info('STARTING HERE')
    logging.info('PARAMETERS => ' + str(args))

    main(args.source + "/Read_Folders.sqlite")

    logging.info('TIME OF EXECUTION = '
                 + secondsToStr(time.time() - start_time))
